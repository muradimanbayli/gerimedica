package nl.gerimedica.service;

import java.io.InputStream;
import java.util.List;
import lombok.RequiredArgsConstructor;
import nl.gerimedica.entity.SymptomEntity;
import nl.gerimedica.exception.NotFoundException;
import nl.gerimedica.exception.UnsupportedFormatException;
import nl.gerimedica.mapper.SymptomMapper;
import nl.gerimedica.model.SymptomDto;
import nl.gerimedica.repository.SymptomRepository;
import nl.gerimedica.service.reader.SymptomFileReader;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SymptomService {
    private final SymptomRepository symptomRepository;
    private final List<SymptomFileReader> readers;

    public List<SymptomDto> uploadData(InputStream inputStream, String mediaType) {
        SymptomFileReader symptomFileReader = readers.stream()
            .filter(r -> r.isSupported(mediaType))
            .findFirst()
            .orElseThrow(() -> new UnsupportedFormatException(mediaType));
        List<SymptomEntity> entities = symptomFileReader.parse(inputStream);
        List<SymptomEntity> savedEntities = symptomRepository.saveAll(entities);
        return SymptomMapper.INSTANCE.entityToDto(savedEntities);
    }

    public List<SymptomDto> getAllSymptom() {
        List<SymptomEntity> entities = symptomRepository.findAll();
        return SymptomMapper.INSTANCE.entityToDto(entities);
    }

    public SymptomDto getSymptomByCode(String code) {
        SymptomEntity entity = symptomRepository.findById(code)
                .orElseThrow(() -> new NotFoundException("Not found code:" + code));
        return SymptomMapper.INSTANCE.entityToDto(entity);
    }

    public void deleteAll() {
        symptomRepository.deleteAll();
    }
}
