package nl.gerimedica.service.reader;

import java.io.InputStream;
import java.util.List;

import nl.gerimedica.entity.SymptomEntity;

public interface SymptomFileReader {
  boolean isSupported(String mediaType);
  List<SymptomEntity> parse(InputStream inputStream);
}
