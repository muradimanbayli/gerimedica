package nl.gerimedica.controller;

import java.io.IOException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import nl.gerimedica.model.SymptomDto;
import nl.gerimedica.service.SymptomService;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "symptoms")
@RequiredArgsConstructor
public class SymptomController {
    private final SymptomService symptomService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<List<SymptomDto>> upload(@RequestParam("file") MultipartFile file) throws IOException {
        return ResponseEntity.ok(symptomService.uploadData(file.getInputStream(), file.getContentType()));
    }

    @GetMapping
    public ResponseEntity<List<SymptomDto>> getAll() {
        return ResponseEntity.ok(symptomService.getAllSymptom());
    }

    @GetMapping("/{code}")
    public ResponseEntity<SymptomDto> getSingle(@PathVariable("code") String code){
       return ResponseEntity.ok(symptomService.getSymptomByCode(code));
    }

    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        symptomService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
