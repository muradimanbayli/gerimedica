package nl.gerimedica.service.reader;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.gerimedica.entity.SymptomEntity;

class SymptomCSVFileReaderTest {

  SymptomCSVFileReader symptomCSVFileReader = new SymptomCSVFileReader();

  @Test
  void isSupportedGivenNoneSupportedMediaTypeExpectFalse() {
    //setup
    String mediaTypeFake = "Fake";
    //when
    boolean result = symptomCSVFileReader.isSupported(mediaTypeFake);
    //expect
    Assertions.assertThat(result).isFalse();
  }

  @Test
  void isSupportedGivenCSVMediaTypeExpectTrue() {
    //setup
    String mediaTypeTextCSV = "text/csv";
    //when
    boolean result = symptomCSVFileReader.isSupported(mediaTypeTextCSV);
    //expect
    Assertions.assertThat(result).isTrue();
  }


  @Test
  void parseGivenValidCSVFileExpectOneEntityRetrieve() {
    //setup
    InputStream inputStream = new ByteArrayInputStream(VALID_CSV_FILE.getBytes());
    //when
    List<SymptomEntity> entities = symptomCSVFileReader.parse(inputStream);
    //expect
    Assertions.assertThat(entities).hasSize(1);
  }

  private final static String VALID_CSV_FILE = "\"source\",\"codeListCode\",\"code\",\"displayValue\",\"longDescription\",\"fromDate\",\"toDate\",\"sortingPriority\"\n"
      + "\"ZIB\",\"ZIB001\",\"271636001\",\"Polsslag regelmatig\",\"The long description is necessary\",\"01-01-2019\",\"\",\"1\"";
}