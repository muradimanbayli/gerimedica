package nl.gerimedica.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import nl.gerimedica.entity.SymptomEntity;
import nl.gerimedica.exception.NotFoundException;
import nl.gerimedica.exception.UnsupportedFormatException;
import nl.gerimedica.model.SymptomDto;
import nl.gerimedica.repository.SymptomRepository;
import nl.gerimedica.service.reader.SymptomFileReader;

@ExtendWith(MockitoExtension.class)
class SymptomServiceTest {

  @Mock
  private SymptomRepository symptomRepository;

  @Mock
  private List<SymptomFileReader> readers;

  @InjectMocks
  private SymptomService symptomService;


  @Test
  void getAllSymptomGivenDatasetExpectAllRetrieve() {
    // setup
    List<SymptomEntity> all = Instancio.createList(SymptomEntity.class);
    when(symptomRepository.findAll()).thenReturn(all);
    // when
    List<SymptomDto> result = symptomService.getAllSymptom();
    // expect
    assertThat(result).isNotNull();
    verify(symptomRepository).findAll();
  }

  @Test
  void getSymptomByCodeGiveDataWithCodeExpectOneRetrieve() {
    // setup
    String id = UUID.randomUUID().toString();
    SymptomEntity symptomEntity = Instancio.create(SymptomEntity.class);
    when(symptomRepository.findById(id)).thenReturn(Optional.of(symptomEntity));
    // when
    SymptomDto result = symptomService.getSymptomByCode(id);
    // expect
    assertThat(result).isNotNull();
    verify(symptomRepository).findById(id);
  }

  @Test
  void getSymptomByCodeGiveInvalidIdExpectNotException() {
    // setup
    String id = UUID.randomUUID().toString();
    when(symptomRepository.findById(id)).thenReturn(Optional.empty());
    // when
    NotFoundException exception = assertThrows(NotFoundException.class, () -> symptomService.getSymptomByCode(id));
    // expect
    assertThat(exception).isNotNull();
    assertThat(exception.getMessage()).isEqualTo("Not found code:" + id);
  }

  @Test
  void deleteAllGivenNoneExpectAllDataRemoved() {
    // when
    symptomService.deleteAll();
    // expect
    verify(symptomRepository).deleteAll();
  }

  @Test
  void uploadDataGivenNoneSupportedMediaTypeExpectUnsupportedFormatException() {
    //setup
    ReflectionTestUtils.setField(symptomService, "readers", List.of());
    String fakeMediaType = "I am fake";
    InputStream inputStream = new ByteArrayInputStream("Fake Stream".getBytes());
    // when
    UnsupportedFormatException exception = assertThrows(UnsupportedFormatException.class, () -> symptomService.uploadData(inputStream, fakeMediaType));
    // expect
    assertThat(exception).isNotNull();
    assertThat(exception.getMessage()).isEqualTo("System doesnot support the file format: " +fakeMediaType);
  }

  @Test
  void uploadDataGivenSupportedMediaTypeExpectDataSaved() {
    //setup
    SymptomFileReader symptomFileReader = mock(SymptomFileReader.class);
    ReflectionTestUtils.setField(symptomService, "readers", List.of(symptomFileReader));
    String csvMediaType = "text/csv";
    when(symptomFileReader.isSupported(csvMediaType)).thenReturn(true);
    InputStream inputStream = new ByteArrayInputStream("Fake Stream".getBytes());
    when(symptomFileReader.parse(inputStream)).thenReturn(Instancio.createList(SymptomEntity.class));
    // when
    List<SymptomDto> result = symptomService.uploadData(inputStream, csvMediaType);
    // expect
    assertThat(result).isNotNull();
    verify(symptomRepository).saveAll(any());
  }
}