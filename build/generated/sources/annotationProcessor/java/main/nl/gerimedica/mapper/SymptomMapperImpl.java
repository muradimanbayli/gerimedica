package nl.gerimedica.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import nl.gerimedica.entity.SymptomEntity;
import nl.gerimedica.model.SymptomDto;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-15T14:12:33+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 19.0.1 (Oracle Corporation)"
)
public class SymptomMapperImpl implements SymptomMapper {

    @Override
    public SymptomDto entityToDto(SymptomEntity entity) {
        if ( entity == null ) {
            return null;
        }

        SymptomDto symptomDto = new SymptomDto();

        symptomDto.setSource( entity.getSource() );
        symptomDto.setCodeListCode( entity.getCodeListCode() );
        symptomDto.setDisplayValue( entity.getDisplayValue() );
        symptomDto.setCode( entity.getCode() );
        symptomDto.setLongDescription( entity.getLongDescription() );
        symptomDto.setFromDate( entity.getFromDate() );
        symptomDto.setToDate( entity.getToDate() );
        symptomDto.setSortingPriority( entity.getSortingPriority() );

        return symptomDto;
    }

    @Override
    public List<SymptomDto> entityToDto(List<SymptomEntity> entities) {
        if ( entities == null ) {
            return null;
        }

        List<SymptomDto> list = new ArrayList<SymptomDto>( entities.size() );
        for ( SymptomEntity symptomEntity : entities ) {
            list.add( entityToDto( symptomEntity ) );
        }

        return list;
    }
}
